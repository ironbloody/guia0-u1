#include <iostream>
using namespace std;
#include "Cadena.h"
#include "Aminoacido.h"
#include "Proteina.h"

Cadena::Cadena() {
    string letra = "";
    list<Aminoacido> aminoacidos;
}

Cadena::Cadena (string letra, list<Aminoacido> aminoacidos) {
    this -> letra = letra;
    this -> aminoacidos = aminoacidos;
}
// Metodos set
void Cadena::set_letra(string letra) {
  this -> letra = letra;
}

// Funcion para añadir aminoacidos a la proteina
void Cadena::add_aminoacido() {
  string nombre;
  list<Aminoacido> aminoacidos;
  int numero;
  
  Aminoacido p = Aminoacido();
  cout<< "ingrese el numero de aminoacidos" << endl;
  cin >> numero;
  p.set_numero(numero);
  for (int i=0; i<numero; i++){
    cout << "Ingrese el aminoacido " << i+1 << ": " << endl;
    cin >> nombre;
    p.set_nombre(nombre);
    p.add_atomo();
    aminoacidos.push_back(p);
  }
  this -> aminoacidos = aminoacidos;
}
// Metodos get
string Cadena::get_letra() {
  return this -> letra;
}

list<Aminoacido> Cadena::get_aminoacidos(){
  return this -> aminoacidos;
}
