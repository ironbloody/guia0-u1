#include <list>
#include <iostream>
#include "Proteina.h"
#include "Cadena.h"
#include "Aminoacido.h"
#include "Atomo.h"
#include "Coordenada.h"
using namespace std;

class Ingresar {
  public:
    list<Proteina> proteinas;
    
    list<Proteina> get_proteina(){
      return this -> proteinas;
    }
};
// Impresion de datos
void print_proteina (list<Proteina> proteinas) {
  for (Proteina p: proteinas) {
    cout << "Proteina: " << p.get_nombre() << "\n"<< "ID: " << p.get_id() << endl;
    for (Cadena c: p.get_cadenas()){
      cout << "Cadena: " << c.get_letra() << endl;
      for (Aminoacido a: c.get_aminoacidos()){
        cout << "Aminoacido: " << a.get_nombre() << endl;
        for (Atomo b: a.get_atomos()){
          cout << "Atomo: " << b.get_nombre() << endl;
          cout << "Coordenadas: \n";
          cout << "x: " <<b.get_coordenada().get_x() << endl;
          cout << "y: " <<b.get_coordenada().get_y() << endl;
          cout << "z: " <<b.get_coordenada().get_z() << endl;
        }
      }
    }
  }
}

// Creacion lista proteina
list<Proteina> ingresar_proteina (list<Proteina> proteinas) {
  string nombre;
  string id;
  Proteina p = Proteina();
  cout << "Ingrese el nombre de la proteina: " << endl;
  cin >> nombre;
  cout << "Ingrese la ID: " << endl;
  cin >> id;
  p.set_nombre(nombre);
  p.set_id(id);
  p.add_cadena();
  proteinas.push_back(p);
  return proteinas;
}

// Main que contiene el menu y llama las demas funciones
int main()
{
  Proteina();
  Ingresar p = Ingresar();
  list<Proteina> proteinas = p.get_proteina();
  int opcion;
  
  do {
    cout << "---------------------------" << endl;
    cout << "1. Ingresar proteina" << endl;
    cout << "2. Imprimiar proteina" << endl;
    cout << "3. salir" << endl;
    cout << "---------------------------" << endl;
    
    cout << "\nIngrese una opcion: ";
    cin >> opcion;
    
    switch (opcion) {
    case 1:
      proteinas = ingresar_proteina (proteinas);
      break;
      
      
    case 2:
      print_proteina (proteinas);
      break;
      
    }        
  } while (opcion != 3);
  return 0;
}