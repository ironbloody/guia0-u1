#include <iostream>
#include <list>
#include "Cadena.h"
using namespace std;

#ifndef PROTEINA_H
#define PROTEINA_H

class Proteina {
  
  // Metodos privados
  private:
    string nombre = "";
    string id= "";
    list<Cadena> cadenas;
    
  // Metodos publicos
  public:
    Proteina();
    Proteina (string nombre, string id, list<Cadena> cadenas);
    
    // Set y get
    void set_nombre(string nombre);
    void set_id(string id);
    void set_cadena(list<Cadena> cadenas);
    void add_cadena();
    string get_id();
    string get_nombre();
    list<Cadena> get_cadenas();
   
};
#endif  
    
