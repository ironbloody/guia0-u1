#include <iostream>
#include <list>
#include "Atomo.h"
using namespace std;

#ifndef AMINOACIDO_H
#define AMINOACIDO_H

class Aminoacido {
  
  // Metodos privados
  private:
    string nombre = "";
    int numero = 0;
    list<Atomo> atomos;
    
  // Metodos publicos
  public:
    Aminoacido ();
    Aminoacido (string nombre, int numero, list<Atomo> atomos);
    
    // Set y get
    void set_nombre(string nombre);
    void set_numero(int numero);
    void add_atomo();
    string get_nombre();
    int get_numero();
    list<Atomo> get_atomos();
   
};
#endif  
    
