#include <iostream>
#include "Coordenada.h"
using namespace std;

#ifndef ATOMO_H
#define ATOMO_H

class Atomo {
  
  // Metodos privados
  private:
    string nombre = "";
    int numero = 0;
    Coordenada coordenada;
    
  // Metodos publicos
  public:
    Atomo ();
    Atomo (string nombre, int numero, Coordenada coordenada);
    
    // Set y get
    void set_nombre(string nombre);
    void set_numero(int numero);
    void set_coordenada(Coordenada coordenada);
    string get_nombre();
    int get_numero();
    Coordenada get_coordenada();
   
};
#endif  

