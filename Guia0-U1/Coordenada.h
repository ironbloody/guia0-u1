#include <iostream>
using namespace std;

#ifndef COORDENADA_H
#define COORDENADA_H

class Coordenada {
  
  // Metodos privados
  private:
    float x = 0;
    float y = 0;
    float z = 0;
    
  // Metodos publicos
  public:
    Coordenada ();
    Coordenada (float x, float y, float z);
    
    // Set y get
    void set_x(float x);
    void set_y(float y);
    void set_z(float z);
    float get_x();
    float get_y();
    float get_z();
   
};
#endif  
    
