#include <iostream>
using namespace std;
#include "Aminoacido.h"
#include "Atomo.h"
#include "Coordenada.h"

Aminoacido::Aminoacido() {
    string nombre = "";
    int numero= 0;
    list<Atomo> atomos;
}

Aminoacido::Aminoacido (string nombre, int numero, list<Atomo> atomos) {
  this -> nombre = nombre;
  this -> numero = numero;
  this -> atomos = atomos;
}

// metodos set
void Aminoacido::set_nombre(string nombre) {
  this->nombre = nombre;
}

void Aminoacido::set_numero(int numero) {
  this->numero = numero;
}

// Funcion para añadir atomos  a la proteina
void Aminoacido::add_atomo() {
  string nombre;
  float x;
  float y;
  float z;
  int numero;
  list<Atomo> atomos;
  
  Atomo a = Atomo();
  Coordenada c = Coordenada();
  
  cout<< "ingrese el numero de atomos:" << endl;
  cin >> numero;
  a.set_numero(numero);
  for (int i=0; i<numero; i++){
    cout << "Ingrese el atomo " << i+1 << ": " << endl;
    cin >> nombre;
    a.set_nombre(nombre);
    cout << "Ingrese las coordenadas del atomo " << i+1 << ": " << endl;
    cin >> x;
    cin >> y;
    cin >> z;
    c.set_x(x);
    c.set_y(y);
    c.set_z(z);
    a.set_coordenada(c);
    atomos.push_back(a);
  }
  this -> atomos = atomos;
}

//Metodos set
string Aminoacido::get_nombre() {
  return this->nombre;
}

int Aminoacido::get_numero() {
  return this->numero;
}

list<Atomo> Aminoacido::get_atomos(){
  return this -> atomos;
}
