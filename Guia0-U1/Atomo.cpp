#include <iostream>
using namespace std;
#include "Atomo.h"
#include "Coordenada.h"

Atomo::Atomo() {
    string nombre = "";
    int numero= 0;
    Coordenada coordenada;
}

Atomo::Atomo(string nombre, int numero, Coordenada coordenada) {
  this->nombre = nombre;
  this->numero = numero;
  this->coordenada = coordenada;
}
// Metodos set and get
void Atomo::set_nombre(string nombre) {
  this->nombre = nombre;
}

void Atomo::set_numero(int numero) {
  this->numero = numero;
} 

void Atomo::set_coordenada(Coordenada coordenada){
  this -> coordenada = coordenada;
}

string Atomo::get_nombre() {
  return this->nombre;
}

int Atomo::get_numero() {
  return this->numero;
}

Coordenada Atomo::get_coordenada(){
  return this -> coordenada;
}
