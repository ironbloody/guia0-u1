#include <iostream>
#include <list>
#include "Aminoacido.h"
using namespace std;

#ifndef CADENA_H
#define CADENA_H

class Cadena {
  
  // Metodos privados
  private:
    string letra = "";
    list<Aminoacido> aminoacidos;
    
  // Metodos publicos
  public:
    Cadena();
    Cadena (string letra, list<Aminoacido> aminoacidos);
    
    // Set y get
    void set_letra(string letra);
    void add_aminoacido();
    string get_letra();
    list<Aminoacido> get_aminoacidos();
   
};
#endif  
    
