#include <iostream>
using namespace std;
#include "Proteina.h"
#include "Cadena.h"

Proteina::Proteina() {
    string nombre = "";
    int id= 0;
}

Proteina::Proteina (string nombre, string id, list<Cadena> cadenas) {
  this->nombre = nombre;
  this->id = id;
  this->cadenas = cadenas;
}
// Metodos set
void Proteina::set_nombre(string nombre) {
  this->nombre = nombre;
}

void Proteina::set_cadena(list<Cadena> cadenas){
  this -> cadenas = cadenas;
}

void Proteina::set_id(string id) {
  this->id = id;
}

// Funcion para añadir una cadena a la proteina
void Proteina::add_cadena() {
  string letra;
  Cadena p = Cadena();

  cout << "Ingrese la cadena: " << endl;
  cin >> letra;
  p.set_letra(letra);
  p.add_aminoacido();
  cadenas.push_back(p);
  this -> cadenas = cadenas;
}

// Metodos get
string Proteina::get_nombre() {
  return this->nombre;
}

string Proteina::get_id() {
  return this->id;
}

list<Cadena> Proteina::get_cadenas(){
  return this->cadenas;
}


