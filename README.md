**Guia 0 Unidad 1**                                                                                             
Programa basado en programación orientada a objetos.                                                                           

**Pre-requisitos**                                                                                                                                    
Cplusplus                                                                                                                                                
Make                                                                                                                                             
Ubuntu                                                                                                                                                                                                                                                                            
**Instalación**                                                                                                                                       
Instalar Make si no estan en su computador.                                

Para instalar Make inicie la terminal y escriba lo siguiente:

sudo apt install make

**Ejecutando**                                                                                                                                         
Para abrir el programa necesita ejecutar el archivo make desde la terminal, luego escribir en la terminal lo siguiente: ./programa.
Una vez abierto encontrara un menu con 3 opciones diferentes, la opcion 1 le serviera para ingresar todos los datos de la proteina, la opcion 2 le servirara para visualizar los datos y la ultima para salir del programa 

**Construido con**                                                                                                                                    
C++
Librerias:
Iostream
List                                                                                                                               

**Versionado**                                                                                                                                        
Version 1.7                                                                                                                                        

**Autores**                                                                                                                                                                                                                                                 
Rodrigo Valenzuela                                                                                                                                                                                               
